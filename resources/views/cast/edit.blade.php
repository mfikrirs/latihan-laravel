@extends('layout.master')
@section('title')
Halaman Edit Cast
@endsection
@section('content')

<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
  <div class="form-group">
    <label>Nama</label>
    <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
  </div>
  @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="integer" class="form-control" name="umur" value="{{$cast->umur}}">
  </div>
  @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" cols="30" rows="10" class="form-control" value="{{$cast->bio}}"></textarea>
  </div>
  @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
  @enderror


  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection