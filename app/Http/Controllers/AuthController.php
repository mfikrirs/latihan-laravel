<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function kirim(Request $request)
    {
       // dd($request->all());
       $namaDepan = $request['first_name'];
       $namaBelakang = $request['last_name'];

       return view('page.welcome', compact("namaDepan", "namaBelakang"));
    }
}
