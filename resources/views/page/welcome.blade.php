@extends('layout.master')
@section('title')
Halaman Index
@endsection
@section('content')
    <h1>Selamat Datang! {{$namaDepan}} {{$namaBelakang}}</h1>
    <h4>Terima kasih telah bergabung di Website Kami. Media belajar kita bersama!</h4>
@endsection