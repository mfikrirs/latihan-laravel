@extends('layout.master')
@section('title')
Halaman Form
@endsection
@section('content')
    <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4>
        <form action="/kirim" method="POST">
            @csrf
            <label>First name :</label> <br>
            <input type="text" name="first_name"> <br><br>
            <label>Last name :</label> <br>
            <input type="text" name="last_name"> <br><br>
            <label>Gender</label> <br>
            <input type="radio" name="gender" value="101">Male <br>
            <input type="radio" name="gender" value="102">Female <br><br>
            <label>Nationality</label><br>
            <select name="nationality">
                <option value="indonesia">Indonesia</option>
                <option value="england">Amerika</option>
                <option value="other">Inggris</option>
            </select><br><br>
            <label>Language Spoken</label><br>
            <input type="checkbox" name="language" value="121">Bahasa Indonesia <br>
            <input type="checkbox" name="language" value="122">English <br>
            <input type="checkbox" name="language" value="123">Other <br><br>
            <label>Bio</label><br>
            <textarea name="bio" cols="30" rows="10"></textarea><br><br>
            <input type="submit" value="Sign Up">

        </form>
@endsection